<?php

require_once(__DIR__.'/../lib/inc/constants.php');
require_once(__DIR__.'/../lib/controller/CarsCnt.php');

session_start();

$cnt = new CarsCnt();
$drs = $cnt->carList();

$title_pag = "Car list";

?><html>
<?php include_once(__DIR__.'/../lib/inc/head.php'); ?>
  <body>
    <div class="contenedor">
      <a href="/add.php">Add Car</a>
      <h1><?=$title_pag?></h1>
      <table>
        <tr> 
          <th>ID</th>
          <th>Car</th>
          <th>Year</th>
          <th>&nbsp;</th>
        </tr>
<?php foreach($drs as $dr){ ?>
        <tr>
          <td><?=$dr->getBid()?></td>
          <td><?=$dr->getCar()?></td>
          <td><?=$dr->getYear()?></td>
          <td><a href="/details.php?index=<?=$dr->getBid()?>">Info Vehicle</a></td>
          <td><a href="/update.php?index=<?=$dr->getBid()?>">Update</a></td>
          <td><a href="/forms/delete.php?index=<?=$dr->getBid()?>">Delete</a></td>
        </tr>
<?php } ?>
      </table>
     <?php include('footer.php');?>
     </div>
  </body>
</html>