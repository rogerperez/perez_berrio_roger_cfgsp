<?php

require_once(__DIR__.'/../lib/inc/constants.php');
require_once(__DIR__.'/../lib/controller/CarsCnt.php');

session_start();

$ndx = $_GET['index'];

$cnt = new CarsCnt();
$dr = $cnt->carDetails($ndx);
$id_imatge = $dr->getImg();
$id_video = $dr->getVideo();
$title_pag = "Car details";

?><html>
<?php include_once(__DIR__.'/../lib/inc/head.php'); ?>
  <body>
    <div class="contenedor">
      <h1><?=$title_pag?></h1>
      <dl>
        <dt>Car</dt>
        <dd><?=$dr->getCar()?>
        <dt>Year</dt>
        <dd><?=$dr->getYear()?>
        <dt>Info</dt>
        <dd><?=$dr->getInfo()?>
        <dt>Img</dt>
        <dd><?php include(__DIR__.'/../lib/inc/google.php');?>
        <dt>Video</dt>
        <dd><?php include(__DIR__.'/../lib/inc/youtube.php');?>
        <dt>Data de modificació</dt>
        <dd><?=$dr->getDate()?>
        <dt>Autor</dt>
        <dd><?=$dr->getAutor()?>
      </dl>
      <a href="/">Back to index</a>
    </div>
  </body>
</html>