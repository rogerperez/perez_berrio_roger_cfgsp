<?php

$title_pag = "Car add";

?><html>
<?php include_once(__DIR__.'/../lib/inc/head.php'); ?>
  <body>
    <div class="contenedor">
      <h1><?=$title_pag?></h1>
      <form action="/forms/add.php" method="post">
      <dl>
        <dt>Car</dt>
        <dd><input type="text" name="car"/></dd>
        <dt>Year</dt>
        <dd><input type="text" name="year"/></dd>
        <dt>Info</dt>
        <dd><textarea type="text" name="info"></textarea></dd>
        <dt>Img</dt>
        <dd><input type="text" name="img"/></dd>
        <dt>Video</dt>
        <dd><input type="text" name="video"/></dd>
        <dt>Data de creació</dt>
        <dd><input type="date" name="fecha" value="<?php echo date("Y-m-d");?>"></dd>
        <dt>Autor</dt>
        <dd><input type="text" name="autor"/></dd>
        <dd><input type="submit" name="cs" value="Crear"/></dd>
      </dl>
      </form>
      <a href="/">Back home</a>
    </div>
  </body>
</html>