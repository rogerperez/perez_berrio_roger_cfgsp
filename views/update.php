<?php

require_once(__DIR__.'/../lib/inc/constants.php');
require_once(__DIR__.'/../lib/controller/CarsCnt.php');

session_start();

$ndx = $_GET['index'];

$cnt = new CarsCnt();
$dr = $cnt->carDetails($ndx);

$title_pag = "Car update";

?><html>
<?php include_once(__DIR__.'/../lib/inc/head.php'); ?>
  <body>
    <div class="contenedor">
      <h1><?=$title_pag?></h1>
      <form action="/forms/update.php" method="post">
      <dl>
        <dt>Car</dt>
        <dd><input type="text" name="car" value="<?=$dr->getCar()?>"/></dd>
        <dt>Year</dt>
        <dd><input type="text" name="year" value="<?=$dr->getYear()?>"/></dd>
        <dt>Info</dt>
        <dd><textarea type="text" name="info"  value="<?=$dr->getInfo()?>"/><?=$dr->getInfo()?></textarea></dd>
        <dt>Img</dt>
        <dd><input type="text" name="img" value="<?=$dr->getImg()?>"/></dd>
        <dt>Video</dt>
        <dd><input type="text" name="video" value="<?=$dr->getVideo()?>"/></dd>
        <dd>
        <dt>Data de creació</dt>
        <dd><input type="date" name="fecha" value="<?php echo date("Y-m-d");?>"></dd>
        <dt>Autor</dt>
        <dd><input type="text" name="autor" value="<?=$dr->getAutor()?>"/></dd>
        <dd>
          <input type="hidden" name="cari" value="<?=$ndx?>"/>
          <input type="submit" name="cs" value="Update"/>
        </dd>
      </dl>
      </form>
      <a href="/">Back home</a>
    </div>
  </body>
</html>