<?php

require_once(__DIR__.'/../../lib/inc/constants.php');
require_once(__DIR__.'/../../lib/controller/CarsCnt.php');

session_start();

$ndx = $_GET['index'];

$cnt = new CarsCnt();
$dr = $cnt->removeCarByIndex($ndx);

$title_pag = "Car deleted";

?><html>
<?php include_once(__DIR__.'/../../lib/inc/head.php'); ?>
  <body>
    <div class="contenedor">
      <h1><?=$title_pag?></h1>
      <span>This car has been deleted</span>
      <dl>
        <dt>Car</dt>
        <dd><?=$dr->getCar()?>
        <dt>Year</dt>
        <dd><?=$dr->getYear()?>
        <dt>Info</dt>
        <dd><?=$dr->getInfo()?>
        <dt>Imagen</dt>
        <dd><?=$dr->getImg()?>
        <dt>Video</dt>
        <dd><?=$dr->getVideo()?>
        <dt>Data de modificació</dt>
        <dd><?=$dr->getDate()?>
        <dt>Autor</dt>
        <dd><?=$dr->getAutor()?>
      </dl>
      <a href="/">Back to index</a>
    </div>
  </body>
</html>