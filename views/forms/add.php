<?php

require_once(__DIR__.'/../../lib/inc/constants.php');
require_once(__DIR__.'/../../lib/controller/CarsCnt.php');

session_start();

$car = $_POST['car'];
$year = $_POST['year'];
$info = $_POST['info'];
$img = $_POST['img'];
$video = $_POST['video'];
$date = $_POST['fecha'];
$autor = $_POST['autor'];

$cnt = new CarsCnt();
$dr = $cnt->addCar($car, $year, $info, $img, $video, $date, $autor);

$title_pag = "Car successfully added";

?><html>
<?php include_once('/projects/PEREZ_BERRIO_ROGER_CFGSP2/lib/inc/head.php'); ?>
  <body>
    <div class="contenedor">
      <h1><?=$title_pag?></h1>
      <dl>
        <dt>Car</dt>
        <dd><?=$dr->getCar()?>
        <dt>Year</dt>
        <dd><?=$dr->getYear()?>
        <dt>Info</dt>
        <dd><?=$dr->getInfo()?>
        <dt>Img</dt>
        <dd><?=$dr->getImg()?>
        <dt>Video</dt>
        <dd><?=$dr->getVideo()?>
        <dt>Data de modificació</dt>
        <dd><?=$dr->getDate()?>
        <dt>Autor</dt>
        <dd><?=$dr->getAutor()?>
      </dl>
      <a href="/">Back to index</a>
    </div>
  </body>
</html>