<?php

require_once(__DIR__.'/../model/db/CarsDb.php');
require_once(__DIR__.'/../model/Cars.php');

class CarsCnt{

    public function carList(){
        $db = new CarsDb();
        return $db->listCar();
    }

    public function carDetails($id){
        $db = new CarsDb();
        return $db->getCarById($id);
    }

    public function removeCarByIndex($id){
        $db = new CarsDb();
        return $db->deleteCarById($id);
    }

    public function addCar($xc, $xy, $xi, $xis, $xv, $xd, $xa){
        $cntn = $xc;
        $cntp = $xy;
        $cntr = $xi;
        $cntcs = $xis;
        $cntv = $xv;
        $cntd = $xd;
        $cnta = $xa;

        $db = new CarsDb();
        return $db->insertCar($cntn, $cntp, $cntr, $cntcs, $cntv, $cntd, $cnta);
    }

    public function updateCar($xc, $xy, $xi, $xis, $xv,$xd, $xa, $xin){
        $cntn = $xc;
        $cntp = $xy;
        $cntr = $xi;
        $cntcs = $xis;
        $cntv = $xv;
        $cntd = $xd;
        $cnta = $xa;
        $cnti = $xin;

        $db = new CarsDb();
        return $db->updateCar($cntn, $cntp, $cntr, $cntcs, $cntv, $cntd, $cnta, $cnti);
    }
}