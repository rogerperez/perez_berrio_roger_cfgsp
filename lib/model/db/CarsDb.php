<?php

require_once(__DIR__.'/../Cars.php');

class CarsDb{

  private $_conn;

  public function listCar(){
    $this->openConnection();

    $query = "SELECT * FROM ".DDBB_TABLE;
    $stmt = $this->_conn->prepare($query);

    $stmt->execute();
    $res = $stmt->get_result();

    $blts = array();
    while ($blt = $res->fetch_assoc() ) {
      array_push($blts, new Cars($blt['car'],$blt['year'],$blt['info'], $blt['img'], $blt['video'], $blt['date'], $blt['autor'], $blt['bid']));
    }
    return $blts;
  }

  public function getCarById($i){
    $this->openConnection();

    $query = "SELECT * FROM ".DDBB_TABLE." WHERE bid = ?";
    $stmt = $this->_conn->prepare($query);
    $stmt->bind_param("i", $index);
    $index = $i;

    $stmt->execute();
    $res = $stmt->get_result();

    $blt = $res->fetch_assoc();
    return new Cars($blt['car'],$blt['year'],$blt['info'],$blt['img'],$blt['video'], $blt['date'], $blt['autor'],$blt['bid']);
  }


  public function updateCar($c, $y, $im, $is, $v, $d, $a, $i){
    $this->openConnection();

    $query = "UPDATE ".DDBB_TABLE." SET car = ?, year = ?, info = ?, img = ?, video = ?, date = ?, autor = ? WHERE bid = ?";
    $stmt = $this->_conn->prepare($query);
    $stmt->bind_param("sssssssi", $sqlc, $sqly, $sqlim, $sqlis, $sqlv, $sqld, $sqla, $sqli);
    $sqlc = $c;
    $sqly = $y;
    $sqlim = $im;
    $sqlis = $is;
    $sqlv = $v;
    $sqld = $d;
    $sqla = $a;
    $sqli = $i;

    $stmt->execute();

    return $this->getCarById($i);
  }


  public function deleteCarById($i){
    $this->openConnection();

    $vehicle = $this->getCarById($i);

    $query = "DELETE FROM ".DDBB_TABLE." WHERE bid = ?";
    $stmt = $this->_conn->prepare($query);
    $stmt->bind_param("i", $index);
    $index = $i;

    $stmt->execute();

    return $vehicle;
  }


  public function insertCar($c, $y, $im, $is, $v, $d, $a){
    $this->openConnection();

    $query = "INSERT INTO ".DDBB_TABLE." (car, year, info, img, video, date, autor) VALUES (?,?,?,?,?,?,?)";
    $stmt = $this->_conn->prepare($query);
    $stmt->bind_param("sssssss", $sqlc, $sqly, $sqlim, $sqlis, $sqlv, $sqld, $sqla);
    $sqlc = $c;
    $sqly = $y;
    $sqlim = $im;
    $sqlis = $is;
    $sqlv = $v;
    $sqld = $d;
    $sqla = $a;

    $stmt->execute();

    return $this->getCarById($stmt->insert_id);
  }

  private function openConnection(){
    if($this->_conn == NULL){
      $this->_conn = mysqli_connect(DDBB_HOST, DDBB_USER, DDBB_PWD, DDBB_DDBB);
    }
  }

}