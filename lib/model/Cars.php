<?php

class Cars{

    private $_car;
    private $_year;
    private $_info;
    private $_img;
    private $_video;
    private $_date;
    private $_autor;
    private $_bid;

    public function __construct($n, $y, $i, $img, $vid, $date, $autor, $bid = null){
      $this->setCar($n);
      $this->setYear($y);
      $this->setInfo($i);
      $this->setImg($img);
      $this->setVideo($vid);
      $this->setDate($date);
      $this->setAutor($autor);
      $this->_bid = $bid;
    }

    public function getCar(){
        return $this->_car;
    }

    public function getYear(){
        return $this->_year;
    }

    public function getInfo(){
        return $this->_info;
    }

    public function getImg(){
        return $this->_img;
    }

    public function getVideo(){
        return $this->_video;
    }

    public function getBid(){
        return $this->_bid;
    }
    public function getDate(){
        return $this->_date;
    }
    public function getAutor(){
        return $this->_autor;
    }

    public function setDate($date){
        $this->_date = $date;
    }

    public function setAutor($autor){
        $this->_autor = $autor;
    }
    public function setCar($n){
        $this->_car = $n;
    }

    public function setYear($y){
        $this->_year = $y;
    }

    public function setInfo($i){
        $this->_info = $i;
    }

    public function setImg($img){
        $this->_img = $img;
    }

    public function setVideo($vid){
        $this->_video = $vid;
    }


}